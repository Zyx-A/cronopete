# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-12 10:57+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. TRANSLATOR this message is shown in the configuration window to specify that cronopete is in idle state, not doing a backup
#: ../src/backup_base.vala:102
msgid "Ready"
msgstr ""

#. TRANSLATORS This is the name for the backend that allows to store the backups in an external disk
#: ../src/backup_extdisk.vala:43
msgid "Store backups in an external hard disk"
msgstr ""

#: ../src/backup_extdisk.vala:52
msgid "Unmount backup disk"
msgstr ""

#: ../src/backup_extdisk.vala:76
msgid "Storing data in the backup disk"
msgstr ""

#: ../src/backup_extdisk.vala:77
msgid "Wait while the system stores the remaining data."
msgstr ""

#: ../src/backup_extdisk.vala:91
msgid "Can't unmount the backup disk. Another process is using it."
msgstr ""

#: ../src/backup_extdisk.vala:93
msgid "Backup disk unmounted"
msgstr ""

#: ../src/backup_extdisk.vala:94
msgid "Now you can disconnect the backup disk safely."
msgstr ""

#. TRANSLATORS This is the name for the backend that allows to store the backups in a folder, instead of choosing a disk.
#: ../src/backup_folder.vala:32
msgid "Store backups in a folder"
msgstr ""

#. TRANSLATORS This is the text for a Cancel button, that cancels the current action of choosing a folder where to do the backups
#: ../src/backup_folder.vala:69 ../src/options.vala:188 ../src/options.vala:226
msgid "Cancel"
msgstr ""

#. TRANSLATORS This is the text for an Add button, that adds a selected folder as the destination where to do the backups when using the "backup to folder" backend
#: ../src/backup_folder.vala:71 ../src/options.vala:189 ../src/options.vala:227
msgid "Add"
msgstr ""

#: ../src/backup_rsync.vala:108
#, c-format
msgid "Can't create the base folders for backups '%s'."
msgstr ""

#: ../src/backup_rsync.vala:250
#, c-format
msgid "Failed to launch cp to restore: %s"
msgstr ""

#: ../src/backup_rsync.vala:290
msgid "Starting backup"
msgstr ""

#: ../src/backup_rsync.vala:323
msgid "Cleaning incomplete backups"
msgstr ""

#: ../src/backup_rsync.vala:365
#, c-format
msgid "Backup done. Elapsed time: %d:%02d"
msgstr ""

#: ../src/backup_rsync.vala:377
msgid "Failed to rename backup folder. Aborting backup"
msgstr ""

#: ../src/backup_rsync.vala:398
msgid "Backup aborted"
msgstr ""

#: ../src/backup_rsync.vala:435 ../src/backup_rsync.vala:448
#: ../src/backup_rsync.vala:460
#, c-format
msgid "Failed to delete aborted backups: %s"
msgstr ""

#: ../src/backup_rsync.vala:470
#, c-format
msgid "Failed to delete folders: %s"
msgstr ""

#: ../src/backup_rsync.vala:484
#, c-format
msgid "Backing up folder %s"
msgstr ""

#: ../src/backup_rsync.vala:510
#, c-format
msgid "Excluding folder %s"
msgstr ""

#: ../src/backup_rsync.vala:521
#, c-format
msgid "Failed to launch rsync for '%s'. Aborting backup"
msgstr ""

#. if not, then something happened. Let the user know
#: ../src/backup_rsync.vala:555 ../src/backup_rsync.vala:564
#, c-format
msgid "There was a problem when backing up the folder '%s'"
msgstr ""

#: ../src/backup_rsync.vala:579
#, c-format
msgid "Backing up %s"
msgstr ""

#: ../src/backup_rsync.vala:623 ../src/backup_rsync.vala:624
msgid "Syncing disk"
msgstr ""

#: ../src/backup_rsync.vala:630
#, c-format
msgid "Failed to launch sync command: %s"
msgstr ""

#: ../src/backup_rsync.vala:669 ../src/cronopete.vala:285
msgid "Cleaning old backups"
msgstr ""

#: ../src/backup_rsync.vala:681
msgid ""
"The destination disk is too small to hold at least a complete backup while "
"doing another. You must change your destination disk with a bigger one. "
"Aborting."
msgstr ""

#: ../src/backup_rsync.vala:688
msgid "No old backups to delete"
msgstr ""

#: ../src/backup_rsync.vala:698
#, c-format
msgid "Deleting old backup %s"
msgstr ""

#: ../src/backup_rsync.vala:706
#, c-format
msgid "Failed to delete old backup %s: %s"
msgstr ""

#: ../src/choose_disk.vala:65
msgid "Failed to unmount the disk. Aborting format operation."
msgstr ""

#: ../src/choose_disk.vala:92
msgid ""
"Failed to format the disk (maybe it is needing too much time). Please, try "
"again."
msgstr ""

#: ../src/choose_disk.vala:108
msgid "Failed to mount again the disk. Aborting the format operation."
msgstr ""

#: ../src/choose_disk.vala:116
msgid "Failed to get the final UUID. Aborting the format operation."
msgstr ""

#: ../src/choose_disk.vala:132
#, c-format
msgid ""
"The selected drive\n"
"\n"
"%1$s\n"
"\n"
"with a capacity of %2$s must be formated to be used for backups.\n"
"\n"
"To do it, click the <i>Format disk</i> button.\n"
"\n"
"<b>All the data in the drive will be erased</b>"
msgstr ""

#. TRANSLATORS this message says that the current File System (FS) in an external disk is unknown. It is shown when listing the external disks connected to the computer
#: ../src/choose_disk.vala:387
msgid "Unknown FS"
msgstr ""

#. TRANSLATORS Specifies that the size of an external disk is unknown
#: ../src/choose_disk.vala:418
msgid "Unknown size"
msgstr ""

#. There's no disk connected
#. TRANSLATORS Specify that the disk configured for backups is not available
#: ../src/cronopete.vala:262
msgid "Disk not available"
msgstr ""

#. Idle
#. TRANSLATORS The program state, used in a tooltip, when it is waiting to do the next backup
#: ../src/cronopete.vala:270
msgid "Idle"
msgstr ""

#. doing backup, everything is fine
#: ../src/cronopete.vala:280
msgid "Doing backup"
msgstr ""

#. TRANSLATORS The program state, used in a tooltip, when it is doing a backup and there is, at least, a warning message
#: ../src/cronopete.vala:294
msgid "Doing backup, have a warning"
msgstr ""

#. TRANSLATORS The program state, used in a tooltip, when it is doing a backup and there is, at least, an error message
#: ../src/cronopete.vala:300
msgid "Doing backup, have an error"
msgstr ""

#. the backup is disabled
#. TRANSLATORS The program state, used in a tooltip, when the backups are disabled and won't be done
#: ../src/cronopete.vala:307
msgid "Backup is disabled"
msgstr ""

#: ../src/cronopete.vala:324
msgid "Backup incorrect"
msgstr ""

#: ../src/cronopete.vala:325
msgid "There was a problem when doing the last backup. Please, check the log."
msgstr ""

#: ../src/cronopete.vala:327
msgid "View log"
msgstr ""

#. TRANSLATORS Menu entry to start a new backup manually
#: ../src/cronopete.vala:437
msgid "Back Up Now"
msgstr ""

#. TRANSLATORS Menu entry to abort the current backup manually
#: ../src/cronopete.vala:441
msgid "Stop Backing Up"
msgstr ""

#. TRANSLATORS Menu entry to open the interface for restoring files from a backup
#. Create the RESTORE button
#. TRANSLATORS Text for the button that restores the selected files
#: ../src/cronopete.vala:446 ../src/restore.vala:89
msgid "Restore files"
msgstr ""

#. TRANSLATORS Menu entry to open the configuration window
#: ../src/cronopete.vala:461
msgid "Configure the backups"
msgstr ""

#. TRANSLATORS Show the date of the last backup
#: ../src/cronopete.vala:470 ../src/menu.vala:295
#, c-format
msgid "Latest backup: %s"
msgstr ""

#. TRANSLATORS "Not available" refers to a backup (e.g. when the disk is not connected, the backups are not available)
#: ../src/helpers.vala:25
msgid "Not available"
msgstr ""

#. / TRANSLATORS This is used when showing the date of a backup done today. %R is the time when the backup was done
#: ../src/helpers.vala:40
msgid "today at %R"
msgstr ""

#. / TRANSLATORS This is used when showing the date of a backup done yesterday. %R is the time when the backup was done
#: ../src/helpers.vala:44
msgid "yesterday at %R"
msgstr ""

#. / TRANSLATORS This is used when showing the date of a backup done tomorrow (just in case). %R is the time when the backup was done
#: ../src/helpers.vala:48
msgid "tomorrow at %R"
msgstr ""

#. / TRANSLATORS This is used when showing the date of a backup not done today, yesterday nor tomorrow. You can use all the tags in strptime. Adjust the format to the one used in the corresponding country. %B is the month in text format, %e the day in numeric format, %Y is the year in four-digits format, %R is the hour and minute. Results in a text which is like "March 23, 2018 at 18:43". Example of translation for spanish: "%e de %B de %Y a las %R", which gives "23 de marzo de 2018 a las 18:43"
#: ../src/helpers.vala:51
msgid "%B %e, %Y at %R"
msgstr ""

#. TRANSLATOR this is the column in a file manager where file names are displayed
#: ../src/icons_widget.vala:159
msgid "Name"
msgstr ""

#. TRANSLATOR this is the column in a file manager where file types are displayed
#: ../src/icons_widget.vala:167
msgid "Type"
msgstr ""

#. TRANSLATOR this is the column in a file manager where file sizes are displayed
#: ../src/icons_widget.vala:175
msgid "Size"
msgstr ""

#. TRANSLATOR this is the column in a file manager where file modification dates are displayed
#: ../src/icons_widget.vala:181
msgid "Modification date"
msgstr ""

#. TRANSLATOR this is for a popup in a file manager, to choose whether the hidden files in a folder should be shown or not
#: ../src/icons_widget.vala:436
msgid "Show hidden files"
msgstr ""

#. TRANSLATOR this is for a popup in a file manager, to choose to reverse the sorting order
#: ../src/icons_widget.vala:445
msgid "Reverse order"
msgstr ""

#. TRANSLATOR this is for a popup in a file manager, to choose to sort the files by name
#: ../src/icons_widget.vala:454
msgid "Sort by name"
msgstr ""

#. TRANSLATOR this is for a popup in a file manager, to choose to sort the files by type
#: ../src/icons_widget.vala:459
msgid "Sort by type"
msgstr ""

#. TRANSLATOR this is for a popup in a file manager, to choose to sort the files by size
#: ../src/icons_widget.vala:464
msgid "Sort by size"
msgstr ""

#. TRANSLATOR this is for a popup in a file manager, to choose to sort the files by date
#: ../src/icons_widget.vala:469
msgid "Sort by date"
msgstr ""

#. TRANSLATOR this is for a popup in a file manager, to choose to show the files as a bunch of icons
#: ../src/icons_widget.vala:495
msgid "View as icons"
msgstr ""

#. TRANSLATOR this is for a popup in a file manager, to choose to show the files as a list with name, size, type and modification date
#: ../src/icons_widget.vala:500
msgid "View as list"
msgstr ""

#. TRANSLATOR this is the string to put in the list of files, as the type for the folders inside the current folder. It is a file type like "video", "text document", "mp3 audio"...
#: ../src/icons_widget.vala:1028
msgid "Folder"
msgstr ""

#. TRANSLATORS Shows a warning message with "warning" in Orange color
#: ../src/menu.vala:141
#, c-format
msgid "<span foreground=\"#FF7F00\">WARNING:</span> %s"
msgstr ""

#. TRANSLATORS Shows an error message with "error" in Orange color
#: ../src/menu.vala:145
#, c-format
msgid "<span foreground=\"#FF3F3F\">ERROR:</span> %s"
msgstr ""

#. TRANSLATORS This string shows the current status of Cronopete. It can be "Status: idle", or "Status: copying file"...
#: ../src/menu.vala:211
#, c-format
msgid "Status: %s"
msgstr ""

#. TRANSLATORS This text means that the user still has not selected a hard disk where to do the backups
#: ../src/menu.vala:290
msgid "Destination: Not defined"
msgstr ""

#: ../src/menu.vala:292
#, c-format
msgid "Destination: %s"
msgstr ""

#: ../src/menu.vala:294
#, c-format
msgid "Oldest backup: %s"
msgstr ""

#: ../src/menu.vala:302 ../src/menu.vala:304
#, c-format
msgid "Next backup: %s"
msgstr ""

#. TRANSLATORS This string specifies the available and total disk space in back up drive. Example: 43 GB of 160 GB
#: ../src/menu.vala:318
#, c-format
msgid "Available: %lld GB of %lld GB"
msgstr ""

#: ../src/options.vala:58
#, c-format
msgid "Backup hidden files and folders in %s"
msgstr ""

#. Create the EXIT button
#. TRANSLATORS Text for the button that allows to exit the restore window
#: ../src/restore.vala:102
msgid "Exit"
msgstr ""

#: ../src/restore.vala:188
msgid "Aborted"
msgstr ""

#: ../src/restore.vala:192
#, c-format
msgid "Error while restoring %s"
msgstr ""

#: ../src/restore.vala:202
#, c-format
msgid "Restoring file %s"
msgstr ""

#: ../src/restore.vala:208
#, c-format
msgid "Restoring folder %s"
msgstr ""

#. TRANSLATORS Specifies that a rstoring operation has ended
#: ../src/restore.vala:214
msgid "Done"
msgstr ""

#. TRANSLATORS Text for the button in the window that shows a message with the result of a restoring operation, for closing the window
#: ../src/restore.vala:254
msgid "OK"
msgstr ""

#. TRANSLATORS Message shown when the user aborts a restoring operation (when restoring files from a backup to the hard disk)
#: ../src/restore.vala:286
msgid "Aborting restore operation"
msgstr ""
